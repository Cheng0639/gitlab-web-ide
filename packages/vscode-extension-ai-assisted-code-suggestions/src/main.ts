import vscode from 'vscode';
import { GitLabCodeCompletionProvider } from './completion/GitlabCodeCompletionProvider';
import { initializeLogging } from './log';

export async function activate(context: vscode.ExtensionContext) {
  const outputChannel = vscode.window.createOutputChannel('AI assisted code suggestions');
  initializeLogging(line => outputChannel.appendLine(line));

  GitLabCodeCompletionProvider.registerGitLabCodeCompletion(context);
}
