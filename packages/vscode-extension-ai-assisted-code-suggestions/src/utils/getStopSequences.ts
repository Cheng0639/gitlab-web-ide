import * as vscode from 'vscode';

export function getStopSequences(position: number, doc: vscode.TextDocument) {
  const stopSequences: string[] = [];

  if (doc.lineCount === position + 1) {
    return stopSequences;
  }

  const nextLine = doc.lineAt(position + 1).text;
  if (nextLine) {
    stopSequences.push(nextLine.toString());
  }
  return stopSequences;
}
