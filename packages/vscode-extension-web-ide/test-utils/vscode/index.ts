import type * as vscode from 'vscode';

export const createFakeCancellationToken = (): vscode.CancellationToken => ({
  isCancellationRequested: false,
  onCancellationRequested: jest.fn(),
});

export const createFakeProgress = (): vscode.Progress<{ increment: number; message: string }> => ({
  report: jest.fn(),
});
