import { createBaseConfig } from '@gitlab/utils-test';
import handleVSCodeTrackingMessage from './handleVSCodeTrackingMessage';

describe('web-ide/handleVSCodeTrackingMessage', () => {
  describe.each`
    vscodeTrackingEventName      | trackingEvent
    ${'remoteConnectionSuccess'} | ${'remote-connection-success'}
    ${'remoteConnectionFailure'} | ${'remote-connection-failure'}
  `('when $vscodeTrackingEventName is received', ({ vscodeTrackingEventName, trackingEvent }) => {
    it(`calls the handleTracking handler with a ${trackingEvent} tracking event`, () => {
      const config = createBaseConfig();

      handleVSCodeTrackingMessage({ event: { name: vscodeTrackingEventName } }, config);

      expect(config.handleTracking).toHaveBeenCalledWith({ name: trackingEvent });
    });
  });

  describe('when receiving an unknown message', () => {
    it('does not call the handleTracking handler', () => {
      const config = createBaseConfig();

      handleVSCodeTrackingMessage({ event: { name: 'unknown' } }, config);

      expect(config.handleTracking).not.toHaveBeenCalled();
    });
  });
});
